//
// Created by Joseph Graves on 4/17/17.
//

#include "PortScanAnalyzer.h"

PortScanAnalyzer::PortScanAnalyzer(vector<Configuration>* configs, UserAddressList* addresses) : Analyzer()
{
    Addresses = addresses;
    for (int i = 0; i < (*configs).size(); ++i)
    {
        if ((*configs)[i].GetType() == "Likely Attack Port Count")
        {

            LikelyPCount = (*configs)[i].GetValue();
        }
        else if ((*configs)[i].GetType() == "Possible Attack Port Count")
        {

            PossPCount = (*configs)[i].GetValue();
        }
        else
        {
            throw invalid_argument("Invalid Configuration");
        }
    }}

ResultSet PortScanAnalyzer::Run()
{
    ResultSet results;
    vector<UserAddress> tempValue;
    KeyValuePair LiklyAttackers("Likely Attack Port Count", tempValue);
    KeyValuePair PossAttackers("Possible Attack Port Count", tempValue);
    KeyValuePair PortCount("Port Count", tempValue);
    results.data.push_back(LiklyAttackers);
    results.data.push_back(PossAttackers);
    results.data.push_back(PortCount);
    vector<UserAddress> AllAddressesOrganized;
    AllAddressesOrganized.push_back(Addresses->GetAddressFromIndex(0));
    int LikelyIndex, PossIndex, PortIndex;
    for (int j = 0; j < results.data.size(); j++)
    {
        if (results.data[j].GetKey() == "Possible Attack Port Count")
        {
            PossIndex = j;
        } else if (results.data[j].GetKey() == "Likely Attack Port Count")
        {
            LikelyIndex = j;
        } else if (results.data[j].GetKey() == "Port Count")
        {
            PortIndex = j;
        } else {}
    }

    //Organize a list of unique ip addresses with a list of unique Destination Ports and counts
    for (int i = 0; i < Addresses->size(); i++)
    {
        bool added = false;
        for (int j = 0; j < AllAddressesOrganized.size(); ++j)
        {
            if (Addresses->GetAddressFromIndex(i).getSourceAddress() == AllAddressesOrganized[j].getSourceAddress())
            {
                AllAddressesOrganized[j].AddDestPortCount(Addresses->GetAddressFromIndex(i).getDestinationPort());
                added = true;
            }
        }
        if (!added)
        {
            AllAddressesOrganized.push_back(Addresses->GetAddressFromIndex(i));
        }
    }

    //Print out list
//    for (int i = 0; i < AllAddressesOrganized.size(); i++)
//    {
//        cout << i << "| "<< "IP Address: " << AllAddressesOrganized[i].getSourceAddress() << endl;
//        for (int j = 0; j < AllAddressesOrganized[i].DestPortToCount->size(); j++)
//        {
//            cout << "   | " << "Port: " << AllAddressesOrganized[i].GetDestPortFromIndex(j).GetKey() << " | Count: "
//                 << AllAddressesOrganized[i].GetDestPortFromIndex(j).GetCount() << endl;
//        }
//
//        cout << endl << endl;
//    }

        //Check if ip addresses are accessing multiple ports
        for (int i = 0; i < AllAddressesOrganized.size(); i++)
        {
            if (AllAddressesOrganized[i].DestPortToCount->size() >= LikelyPCount)
            {
                //Add to likely port attack vector in ResultSet
                results.data[LikelyIndex].AddValue(AllAddressesOrganized[i]);
            }
            else if (AllAddressesOrganized[i].DestPortToCount->size() >= PossPCount)
            {
                //Add to Possible port attack vector in ResultSet
                results.data[PossIndex].AddValue(AllAddressesOrganized[i]);
            }
            else {}

        }
    vector<int>* Ports;
    Ports = new vector<int>;
    for (int i = 0; i < AllAddressesOrganized.size(); i++)
    {
        //Accessing each UserAddress in list
        for (int j = 0; j < AllAddressesOrganized[i].DestPortToCount->size(); j++)
        {
            //Accessing each list of unique Ports Per IP Address
            if (Ports->size() == 0)
            {
                Ports->push_back(AllAddressesOrganized[i].GetDestPortFromIndex(j).GetKey());
            }
            else
            {
                bool Added = false;
                //Check if port exists if Ports already
                for (int g = 0; g < Ports->size(); g++)
                {
                    if ((*Ports)[g] == AllAddressesOrganized[i].GetDestPortFromIndex(j).GetKey())
                    {
                        Added = true;
                    }
                }
                if (!Added)
                {
                    Ports->push_back(AllAddressesOrganized[i].GetDestPortFromIndex(j).GetKey());
                }
            }


        }
        UserAddress temp;
        temp.SetPorts(Ports);

        results.data[PortIndex].AddValue(temp);
    }

    return results;
}

