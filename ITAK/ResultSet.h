//
// Created by Joseph Graves on 4/12/17.
//

#ifndef ITAK_RESULTSET_H
#define ITAK_RESULTSET_H
#include "Dictionary.h"
#include "UserAddress.h"
#include <string>
#include <vector>
#include "UserAddressList.h"

using namespace std;

class ResultSet : public Dictionary
{
public:
    ResultSet();
    void Add(KeyValuePair dataToAdd);
    void AddValueToKey(string key);
    void PrintDictionary();
    vector<KeyValuePair> data;
    int TimeFrame;
};



#endif //ITAK_RESULTSET_H
