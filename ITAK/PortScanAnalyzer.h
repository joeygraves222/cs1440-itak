//
// Created by Joseph Graves on 4/17/17.
//

#ifndef ITAK_PORTSCANANALYZER_H
#define ITAK_PORTSCANANALYZER_H
#include "KeyValuePair.h"
#include "Analyzer.h"
#include "Configuration.h"
#include <iostream>
using namespace std;

class PortScanAnalyzer : public Analyzer
{
public:
    PortScanAnalyzer(vector<Configuration>* configs, UserAddressList* addresses);
    virtual ResultSet Run();
private:
    UserAddressList* Addresses;
    int LikelyPCount;
    int PossPCount;
    int PortCount;
};


#endif //ITAK_PORTSCANANALYZER_H
