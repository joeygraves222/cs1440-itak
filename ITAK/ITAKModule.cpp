//
// Created by Joseph Graves on 4/17/17.
//

#include "ITAKModule.h"
#include "PortScanAnalyzer.h"
#include "DenialOfServiceAnalyzer.h"


ITAKModule::ITAKModule(ifstream& fin, int Time, int Num, int PossNum, int PortAttacks, int PossPortAttacks)
{
    Addresses = new UserAddressList(fin);
        
    if (Num <= PossNum)
    {
        throw invalid_argument("Invalid Argument: Limit is less than or equal to watchlist limit");
    }
    if (PortAttacks <= PossPortAttacks)
    {
        throw invalid_argument("Invalid Argument: Limit is less than or equal to watchlist limit");
    }
    Configuration TimeFrame("Timeframe", Time);
    Configuration MessageCount("Likely Attack Message Count", Num);
    Configuration PossMCount("Possible Attack Message Count", PossNum);
    DenialOfServiceConfigs = new vector<Configuration>;
    PortScanConfigs = new vector<Configuration>;
    (*DenialOfServiceConfigs).push_back(TimeFrame);
    (*DenialOfServiceConfigs).push_back(MessageCount);
    (*DenialOfServiceConfigs).push_back(PossMCount);
    (*PortScanConfigs).push_back(Configuration("Likely Attack Port Count", PortAttacks));
    (*PortScanConfigs).push_back(Configuration("Possible Attack Port Count", PossPortAttacks));
    DenialAnalyzer = new DenialOfServiceAnalyzer(DenialOfServiceConfigs, Addresses);
    PortAnalyzer = new PortScanAnalyzer(PortScanConfigs, Addresses);
    DenialOfServiceResults = DenialAnalyzer->Run();
    PortScanResults = PortAnalyzer->Run();
}


void ITAKModule::Print()
{
    cout << "--- Denial of Service Analyzer ---" << "\n\n\n";
    DenialOfServiceResults.PrintDictionary();
    cout << "\n\n\n\n\n" << "--- Port Scan Analyzer ---" << "\n\n\n";
    PortScanResults.PrintDictionary();
}

