//
// Created by Joseph Graves on 4/10/17.
//

#ifndef ITAK_USERADDRESSLIST_H
#define ITAK_USERADDRESSLIST_H
#include "UserAddress.h"
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

class UserAddressList
{
private:
    vector<UserAddress> AllAddresses;

public:
    UserAddressList(ifstream& fin);
    void PrintFromIndex(int index);
    UserAddress GetAddressFromIndex(int index);
    UserAddress GetAddressByIP(string IP);
    void PrintAll();
    int size(){return AllAddresses.size();};

};


#endif //ITAK_USERADDRESSLIST_H
