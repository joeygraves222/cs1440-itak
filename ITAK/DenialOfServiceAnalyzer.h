//
// Created by Joseph Graves on 4/17/17.
//

#ifndef ITAK_DENIALOFSERVICEANALYZER_H
#define ITAK_DENIALOFSERVICEANALYZER_H
#include "KeyValuePair.h"
#include "Configuration.h"
#include "Analyzer.h"
#include "KeyCountPair.h"
#include "KeyCountDictionary.h"

class DenialOfServiceAnalyzer : public Analyzer
{
public:
    DenialOfServiceAnalyzer(vector<Configuration>* configs, UserAddressList* addresses);
    ResultSet Run();
    vector<KeyCountDictionary> Timestamps;
//    int CheckTimeStamps(vector<KeyCountPair> stamps);
    int TIMEFRAME;
private:
    UserAddressList* Addresses;

    int likelyMCount;
    int possMCount;
};


#endif //ITAK_DENIALOFSERVICEANALYZER_H
