//
// Created by Joseph Graves on 4/10/17.
//

#include <iostream>
#include "ITAKModuleTester.h"
#include "PortScanAnalyzerTester.h"
#include "DenialOfServiceAnalyzerTester.h"
#include "ResultSetTester.h"

using namespace std;

int main()
{
    cout << "ITAK Test Cases" << endl;
    cout << "Testing ITAK Module..." << endl;
    cout << "     Testing Constructor..." << endl;
    ITAKModuleTester ITAK;
    cout << endl << endl << "     Testing Denial of Service Analyzer..." << endl;
    DenialOfServiceAnalyzerTester DOSTest;
    cout << endl << endl << "     Testing Port Scan Analyzer..." << endl;
    PortScanAnalyzerTester PSATest;
    cout << endl << endl << "     Testing Result Set..." << endl;
    ResultSetTester RSTest;

    if (ITAK.Success && DOSTest.Success && PSATest.Success && RSTest.Success)
    {
        cout << endl << endl << "All Tests Succeeded!" << endl;
    }
    else
    {
        cout << endl << endl << "One or more tests Failed..." << endl;
    }

}