//
// Created by Joseph Graves on 4/21/17.
//

#ifndef ITAK_ITAKMODULETESTER_H
#define ITAK_ITAKMODULETESTER_H
#include "../ResultSet.h"
#include "../ITAKModule.h"
#include "../DenialOfServiceAnalyzer.h"
#include "../PortScanAnalyzer.h"
#include "../Configuration.h"
#include <fstream>
#include <iostream>


class ITAKModuleTester
{
public:
    bool Success = true;
    ITAKModuleTester()
    {
        ifstream fin("../SampleData.csv");
        //Run test cases
        try
        {
            ITAKModule itak1(fin, 3, 5, 3, 10, 7);
            cout << "Constructor test 1 successful..." << endl;
        }
        catch (invalid_argument ex)
        {
            cout << "Constructor Test 1 Failed..." << endl;
            Success = false;
        }


        try
        {
            ITAKModule itak2(fin, 3, 4, 5, 10, 7);
            cout << "Constructor Test 2 Failed..." << endl;
            Success = false;
        }
        catch (invalid_argument ex)
        {
            cout << "Constructor Test 2 Successful..." << endl;

        }

        try
        {
            ITAKModule itak1(fin, 3, 5, 3, 7, 10);
            cout << "Constructor test 3 Failed..." << endl;
            Success = false;
        }
        catch (invalid_argument ex)
        {
            cout << "Constructor Test 3 Successful..." << endl;
        }
    }
};
#endif //ITAK_ITAKMODULETESTER_H
