//
// Created by Joseph Graves on 4/21/17.
//

#ifndef ITAK_DENIALOFSERVICEANALYZERTESTER_H
#define ITAK_DENIALOFSERVICEANALYZERTESTER_H
#include "../DenialOfServiceAnalyzer.h"
#include "../Configuration.h"


class DenialOfServiceAnalyzerTester
{
public:
    bool Success = true;
    DenialOfServiceAnalyzerTester()
    {
        ifstream fin("../SampleData.csv");
        ITAKModule itak(fin, 3, 5, 3, 10, 7);
        try
        {
            DenialOfServiceAnalyzer(itak.DenialOfServiceConfigs, itak.Addresses);
            cout << "Denial of Service Test 1 Successful..." << endl;
        }
        catch (invalid_argument ex)
        {
            cout << "Denial of Service Test 1 Failed..." << endl;
            Success = false;
        }
        vector<Configuration>* configs = new vector<Configuration>;
        Configuration config1("Hello", 1);
        Configuration config2("Hello", 1);
        Configuration config3("Hello", 1);
        configs->push_back(config1);
        configs->push_back(config2);
        configs->push_back(config3);
        try
        {
            DenialOfServiceAnalyzer(configs, itak.Addresses);
            cout << "Denial of Service Test 2 Failed..." << endl;
            Success = false;
        }
        catch (invalid_argument ex)
        {
            cout << "Denial of Service Test 2 Successful..." << endl;
        }
    }
};
#endif //ITAK_DENIALOFSERVICEANALYZERTESTER_H
