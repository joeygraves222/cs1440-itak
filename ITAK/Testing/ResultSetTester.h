//
// Created by Joseph Graves on 4/21/17.
//

#ifndef ITAK_RESULTSETTESTER_H
#define ITAK_RESULTSETTESTER_H
#include "../ResultSet.h"


class ResultSetTester
{
public:
    bool Success = true;
    ResultSetTester()
    {
        ifstream fin("../SampleData.csv");
        ITAKModule itak(fin, 3, 5, 3, 10, 7);
        ResultSet RS = itak.DenialOfServiceResults;
        if (RS.data.size() == 0)
        {
            cout << "Result Set Test 1 Failed..." << endl;
            Success = false;
        }
        else
        {
            cout << "Result Set Test 1 Successful..." << endl;
        }
        ResultSet rs = itak.PortScanResults;
        if (rs.data.size() == 0)
        {
            cout << "Result Set Test 2 Failed..." << endl;
            Success = false;
        }
        else
        {
            cout << "Result Set Test 2 Successful..." << endl;
        }
    }
};
#endif //ITAK_RESULTSETTESTER_H
