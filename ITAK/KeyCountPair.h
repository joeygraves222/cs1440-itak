//
// Created by Joseph Graves on 4/19/17.
//

#ifndef ITAK_KEYCOUNTPAIR_H
#define ITAK_KEYCOUNTPAIR_H


class KeyCountPair
{
private:
    int Key;
    int Count;
public:
    KeyCountPair(int key){Key = key; Count = 1;}
    int GetKey(){return Key;}
    int GetCount(){return Count;}
    void SetKey(int key){Key = key;}
    void IncrementCount(){Count++;}
};


#endif //ITAK_KEYCOUNTPAIR_H
