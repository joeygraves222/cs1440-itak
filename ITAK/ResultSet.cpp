//
// Created by Joseph Graves on 4/12/17.
//

#include "ResultSet.h"






void ResultSet::PrintDictionary()
{

    for (int i = 0; i < data.size(); i++)
    {
        if (data[i].Key == "Attack Periods")
        {
            //Print Attack Periods
            cout << endl << endl << "Key: " << data[i].Key << endl << "Value: ";
            for (int j = 0; j < data[i].Value.size(); j++)
            {
                for (int g = 0; g < data[i].Value[j].AttackPeriods.size(); g++)
                {
                    cout << endl << "       " <<  data[i].Value[j].AttackPeriods[g];
                }
            }
        }
        else if (data[i].Key == "Timeframe")
        {
            //Print Time Frame
            cout << endl << endl << "Key: " << "Timeframe" << endl;
            cout << "Value: " << TimeFrame << endl;
        }
        else if(data[i].Key == "Likely Attack Port Count")
        {
            //Print Attack Ports
            cout << endl << endl << "Key: " << data[i].Key << endl << "Value: ";
            for (int j = 0; j < data[i].Value.size(); j++)
            {
                cout << endl << "       " << data[i].Value[j].getSourceAddress();
            }
        }
        else if (data[i].Key == "Possible Attack Port Count")
        {
            //Print Likely Attack Ports
            cout << endl << endl << "Key: " << data[i].Key << endl << "Value: ";
            for (int j = 0; j < data[i].Value.size(); j++)
            {
                cout << endl << "       " << data[i].Value[j].getSourceAddress();
            }
        }
        else if (data[i].Key == "Port Count")
        {
            //Print List Of Ports
            cout << endl << endl << "Key: " << data[i].Key << endl << "Value: ";
            for (int j = 0; j < data[i].Value[0].Ports->size(); j++)
            {

                cout << endl << "       " << data[i].Value[0].GetPortAtIndex(j);
            }
        }
        else
        {
            //Print Message Count and Possible Message Count
            cout << endl << endl << "Key: " << data[i].Key << endl << "Value: ";
            for (int j = 0; j < data[i].Value.size(); j++)
            {
                cout << endl << "       " << data[i].Value[j].getSourceAddress();
            }
        }

    }
}

ResultSet::ResultSet()
{
    data = this->Dictionary::GetData();
}


void ResultSet::AddValueToKey(string key)
{
    //data[0].
}

void ResultSet::Add(KeyValuePair dataToAdd)
{
    data.push_back(dataToAdd);
}