//
// Created by Joseph Graves on 4/18/17.
//

#include "Dictionary.h"


KeyValuePair Dictionary::GetByIndex(int index)
{
    return data[index];
}

KeyValuePair Dictionary::Find(string key)
{
    for (int i = 0; i < data.size(); i++)
    {
        if (data[i].GetKey() == key)
        {
            return data[i];
        }
    }
    throw invalid_argument("Item not Found");
}

vector<KeyValuePair> Dictionary::GetData()
{
    return data;
}

