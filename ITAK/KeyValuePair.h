//
// Created by Joseph Graves on 4/18/17.
//

#ifndef ITAK_KEYVALUEPAIR_H
#define ITAK_KEYVALUEPAIR_H
#include <string>
#include <vector>
#include "UserAddress.h"
using namespace std;


class KeyValuePair
{
public:
    string Key;
    KeyValuePair();
    vector<UserAddress> Value;
    void AddValue(UserAddress ValueToAdd);
    KeyValuePair(string key, vector<UserAddress> value);
    void SetKey(string key);
    void SetValue(vector<UserAddress> value);
    void AddToValue(UserAddress valueToAdd);
    string GetKey();
    vector<UserAddress> GetValue();

};


#endif //ITAK_KEYVALUEPAIR_H
