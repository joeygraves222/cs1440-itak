//
// Created by Joseph Graves on 4/10/17.
//

#ifndef PROJECT_USERADDRESS_H
#define PROJECT_USERADDRESS_H
#include <string>
#include <iostream>
#include "KeyCountPair.h"
#include <vector>
using namespace std;

class UserAddress {

private:
    int TimeStamp;
    string SourceAddress;
    int SourcePort;
    int DestinationPort;





public:
    vector<string> AttackPeriods;

    vector<KeyCountPair>* TimeStampToCount;
    KeyCountPair GetTimeStampFromIndex(int index)
    {
        return (*TimeStampToCount)[index];
    }

    vector<KeyCountPair>* DestPortToCount;
    KeyCountPair GetDestPortFromIndex(int Index)
    {
        return (*DestPortToCount)[Index];
    }

    UserAddress()
    {
        TimeStamp = 000000;
        SourceAddress = "";
        SourcePort = 000000;
        DestinationPort = 000000;
        TimeStampToCount = new vector<KeyCountPair>;
        TimeStampToCount->push_back(KeyCountPair(TimeStamp));
        DestPortToCount = new vector<KeyCountPair>;
        DestPortToCount->push_back(KeyCountPair(DestinationPort));
    }

    UserAddress(int timeStamp, string srcAddress, int srcPort, int destPort)
    {
        TimeStamp = timeStamp;
        SourceAddress = srcAddress;
        SourcePort = srcPort;
        DestinationPort = destPort;
        TimeStampToCount = new vector<KeyCountPair>;
        TimeStampToCount->push_back(KeyCountPair(TimeStamp));
        DestPortToCount = new vector<KeyCountPair>;
        DestPortToCount->push_back(KeyCountPair(DestinationPort));
    }


    void AddTimeStampCount(int timeStamp)
    {
        if (TimeStampToCount->size() == 0)
        {
            KeyCountPair Time(timeStamp);
            TimeStampToCount->push_back(Time);
        }
        else
        {
            for (int i = 0; i < TimeStampToCount->size(); i++)
            {
                if ((*TimeStampToCount)[i].GetKey() == timeStamp)
                {
//                    TimeStampToCount[i][i
                    (*TimeStampToCount)[i].IncrementCount();
                    return;
                }
            }
            KeyCountPair Time(timeStamp);
            TimeStampToCount->push_back(Time);
        }
    }

    void AddDestPortCount(int DestPort)
    {
        if (DestPortToCount->size() == 0)
        {
            KeyCountPair Port(DestPort);
            DestPortToCount->push_back(Port);
        }
        else
        {
            for (int i = 0; i < DestPortToCount->size(); i++)
            {
                if ((*DestPortToCount)[i].GetKey() == DestPort)
                {
//                    TimeStampToCount[i][i
                    (*DestPortToCount)[i].IncrementCount();
                    return;
                }
            }
            KeyCountPair Port(DestPort);
            DestPortToCount->push_back(Port);
        }
    }


    //Getters
    int getTimeStamp(){return TimeStamp;}
    string getSourceAddress(){return SourceAddress;}
    int getSourcePort(){return SourcePort;}
    int getDestinationPort(){return DestinationPort;}
    //Setters
    void setTimeStamp(int stamp){TimeStamp = stamp;}
    void setSourceAddress(string address){SourceAddress = address;}
    void setSourcePort(int port){SourcePort = port;}
    void setDestinationPort(int port){DestinationPort = port;}
    string PrintIpAddress(){return "IP Address: " + SourceAddress;}
    string Print(){return "Time Stamp: " + to_string(TimeStamp) + "\n" + "Source Address: " + SourceAddress + "\n" + "Source Port: " + to_string(SourcePort) + "\n" + "Destination Port: " + to_string(DestinationPort) + "\n";}
    void PrintAddress(){cout << "Time Stamp: " + to_string(TimeStamp) + "\n" + "Source Address: " + SourceAddress + "\n" + "Source Port: " + to_string(SourcePort) + "\n" + "Destination Port: " + to_string(DestinationPort) + "\n";}

    vector<int>* Ports;

    void SetPorts(vector<int>* ports)
    {
        Ports = ports;
    }
    int GetPortAtIndex(int Index)
    {
        return (*Ports)[Index];
    }
};


#endif //PROJECT_USERADDRESS_H


