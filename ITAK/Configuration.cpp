//
// Created by Joseph Graves on 4/17/17.
//

#include "Configuration.h"

Configuration::Configuration(string type, int value)
{
    this->Type = type;
    this->Value = value;
}


string Configuration::GetType()
{
    return Type;
}

int Configuration::GetValue()
{
    return Value;
}