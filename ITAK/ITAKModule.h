//
// Created by Joseph Graves on 4/17/17.
//

#ifndef ITAK_ITAKMODULE_H
#define ITAK_ITAKMODULE_H
#include "ResultSet.h"
#include "Analyzer.h"
#include "DenialOfServiceAnalyzer.h"
#include "PortScanAnalyzer.h"
#include "Configuration.h"
#include <iostream>

using namespace std;

class ITAKModule
{
public:
    ITAKModule(ifstream& fin, int Time, int Num, int PossNum, int PortAttacks, int PossPortAttacks);
    void Print();
    UserAddressList* Addresses;
    vector<Configuration>* DenialOfServiceConfigs;
    vector<Configuration>* PortScanConfigs;
    ResultSet DenialOfServiceResults;
    ResultSet PortScanResults;
    DenialOfServiceAnalyzer* DenialAnalyzer;
    PortScanAnalyzer* PortAnalyzer;



};


#endif //ITAK_ITAKMODULE_H
