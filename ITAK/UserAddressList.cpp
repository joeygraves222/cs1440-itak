//
// Created by Joseph Graves on 4/10/17.
//

#include "UserAddressList.h"

using namespace std;

UserAddressList::UserAddressList(ifstream& fin)
{
    vector<string> values;
    values.push_back("");values.push_back("");values.push_back("");values.push_back("");
    string tempLine;
    while(!fin.eof())
    {
        for (int i = 0; i < 3; i++)
        {
            getline(fin, tempLine, ',');
            values[i] = tempLine;
        }
        getline(fin, tempLine);
        values[3] = tempLine;
        UserAddress temp(stoi(values[0]), values[1], stoi(values[2]), stoi(values[3]));
        AllAddresses.push_back(temp);

    }

}

void UserAddressList::PrintAll()
{
    for (int i = 0; i < AllAddresses.size(); i++)
    {
        cout << "User Number: " << i + 1 << endl;
        AllAddresses[i].Print();
        cout << endl << endl;
    }

}

UserAddress UserAddressList::GetAddressByIP(string IP)
{
    for (int i = 0; i < AllAddresses.size(); i++)
    {
        if (AllAddresses[i].getSourceAddress() == IP)
        {
            return AllAddresses[i];
        }
    }
}

void UserAddressList::PrintFromIndex(int index)
{
    AllAddresses[index].PrintAddress();
}

UserAddress UserAddressList::GetAddressFromIndex(int index)
{
    return AllAddresses[index];
}