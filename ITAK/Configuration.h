//
// Created by Joseph Graves on 4/17/17.
//

#ifndef ITAK_CONFIGURATION_H
#define ITAK_CONFIGURATION_H
#include "KeyValuePair.h"
#include "UserAddressList.h"
#include <vector>
#include <iostream>
#include <string>
using namespace std;

class Configuration
{
public:
    Configuration(string type, int value);
    string GetType();
    int GetValue();
private:
    int Value;
    string Type;

};


#endif //ITAK_CONFIGURATION_H
