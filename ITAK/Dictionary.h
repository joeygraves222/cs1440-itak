//
// Created by Joseph Graves on 4/18/17.
//

#ifndef ITAK_DICTIONARY_H
#define ITAK_DICTIONARY_H
#include "KeyValuePair.h"
#include <vector>

using namespace std;

class Dictionary
{
private:
    vector<KeyValuePair> data;
public:
    KeyValuePair GetByIndex(int index);
    KeyValuePair Find(string key);
    vector<KeyValuePair> GetData();

};


#endif //ITAK_DICTIONARY_H
