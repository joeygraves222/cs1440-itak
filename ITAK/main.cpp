//
// Created by Joseph Graves on 4/10/17.
//

#include <iostream>
#include <string>
#include "ResultSet.h"
#include "UserAddressList.h"
#include "ITAKModule.h"
using namespace std;


int main()
{
    bool invalid = true;
    int Time, Attacks, PossAttacks, PortAttacks, PossPortAttacks;
    ifstream fin("SampleData.csv");
    while (invalid)
    {
        cout << "--Denial of Service Analyzer--" << endl << endl;
        cout << "What Time Frame would you like to set? (In seconds)" << endl;
        cin >> Time;
        cout << "How many messages per Time Frame should be the limit?" << endl;
        cin >> Attacks;
        cout << "How many messages should put the user on the watch list? (Must be fewer than the limit)" << endl;
        cin >> PossAttacks;
        if (PossAttacks < Attacks)
        {
            invalid = false;
        }
        else
        {
            cout << "Invalid Configurations, limit should be greater than watch list value" << endl;
        }
    }

    invalid = true;

    while(invalid)
    {
        cout << endl << endl << endl;
        cout << "--Port Scan Analyzer--" << endl << endl;
        cout << "How many port calls constitutes an attack?" << endl;
        cin >> PortAttacks;
        cout << "How many port calls puts someone on a watch list? (Must be fewer than  an attack)" << endl;
        cin >> PossPortAttacks;
        if (PossPortAttacks < PortAttacks)
        {
            invalid = false;
        }
        else
        {
            cout << "Invalid Configurations, port calls limit should be greater than watch list value" << endl;
        }
    }

    ITAKModule itak(fin, Time, Attacks, PossAttacks, PortAttacks, PossPortAttacks);

    itak.Print();


    return 0;
}


