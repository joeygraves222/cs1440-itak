//
// Created by Joseph Graves on 4/18/17.
//

#include "KeyValuePair.h"



KeyValuePair::KeyValuePair(string key, vector<UserAddress> value)
{
    Key = key;
    Value = value;
}

void KeyValuePair::SetValue(vector<UserAddress> value)
{
    Value = value;
}

void KeyValuePair::AddValue(UserAddress ValueToAdd)
{
    Value.push_back(ValueToAdd);
}

void KeyValuePair::AddToValue(UserAddress valueToAdd)
{

}

void KeyValuePair::SetKey(string key)
{
    Key = key;
}

vector<UserAddress> KeyValuePair::GetValue()
{
    return Value;
}

string KeyValuePair::GetKey()
{
    return Key;
}