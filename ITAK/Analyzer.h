//
// Created by Joseph Graves on 4/13/17.
//

#ifndef ITAK_ANALYZER_H
#define ITAK_ANALYZER_H
#include "ResultSet.h"
#include "KeyValuePair.h"
#include "Configuration.h"
#include <vector>
#include <iostream>

class Analyzer
{
public:
    Analyzer(){};

    virtual ResultSet Run() = 0;
};


#endif //ITAK_ANALYZER_H
