//
// Created by Stephen Clyde on 4/5/17.
//

#ifndef GENERICINHERITANCE_CONTAINER_H
#define GENERICINHERITANCE_CONTAINER_H

template <typename X>
class Container
{

public:
    void add(X x);

};
#endif //GENERICINHERITANCE_CONTAINER_H
